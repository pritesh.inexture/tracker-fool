from pykeyboard import PyKeyboard
import random
import time

keyboard = PyKeyboard()


def press_cntrl():
    while True:
        time.sleep((random.randint(1, 10)) / 10)
        keyboard.tap_key(keyboard.control_key)
