import os
from sys import platform

if platform == "linux" or platform == "linux2":
    # linux
    os.system('python -m pip --user install --upgrade pip')
elif platform == "win32":
    # Windows...
    os.system('python -m pip install --upgrade pip')

from pip._internal import main as pipmain
import main

pipmain(['install', 'pyHook-1.5.1-cp37-cp37m-win_amd64.whl'])
pipmain(['install', 'pyuserinput'])

main.press_cntrl()
